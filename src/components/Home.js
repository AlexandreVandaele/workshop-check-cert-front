import React from 'react';

export const Home = () => {
    return (
        <div className={"bg-gray-700 text-white h-screen"}>
            <div className={"container mx-auto shadow-2xl bg-gray-600 h-full"}>
                <h1 className={"text-center text-4xl pt-14"}>Recherchez le site de votre choix</h1>
                <div className="pt-14 flex w-10/12 mx-auto">
                    <input className="w-full rounded p-2 text-black" type="text" placeholder="Lien du site" />
                    <button className="bg-red-light hover:bg-red-lighter rounded text-white p-2 pl-4 pr-4">
                        <p className="font-semibold text-xl">Search</p>
                    </button>
                </div>
            </div>
            <div className="resultat">
            </div>
        </div>
    )
}
